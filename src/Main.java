
import java.util.*;
public class Main {

    public static void main(String[] args) {
        System.out.println("Please enter your name");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.next();
        System.out.println("Hello " + name + ", please enter numbers you want to sum and multiply");
        scanner.nextLine();
        String numbers = scanner.nextLine();
        int sum = 0;
        int product = 1;
        String[] arrayOfNumbers = numbers.split(" ");
        for (int i =0 ; i<arrayOfNumbers.length; i++){
            sum += Integer.valueOf(arrayOfNumbers[i]);
            product *= Integer.valueOf(arrayOfNumbers[i]);
        }

        scanner.close();
        System.out.println("Product is: " + product + "\nSum is: " + sum);

    }
}
